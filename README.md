# Gitlab CE + Gitlab CI on Docker

## Up and running
Damit dieses Projekt gestartet werden kann, wird Docker benötigt. 

[Docker](https://www.docker.com) installieren, falls noch nicht geschehen. 


Sollte Docker bereits installiert sein, dann kann im Terminal folgender Befehl abgefeuert werden

`docker-compose up --abort-on-container-exit -d`

`docker-compose stop` fährt die Container wieder herunter


## Gitlab CI registrieren
In der Konsole folgende Befehle ausführen

1. Gitlab Runner Container aufrufen
`docker exec -it gitlab-runner gitlab-runner register`

2. Gitlab Instanz angeben
`Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
 http://127.0.0.1`
 
3. Das Gitlab-CI Token ist in Gitlab Administrationsoberfläche abrufbar
`Please enter the gitlab-ci token for this runner`

4. Dem Runner einen Namen geben
`Please enter the gitlab-ci description for this runner
 [hostame] my-runner`

 5. Als nächstes muss ein / mehrere Tag(s) mit dem Gitlab Runner assoziert werden
 `node` oder `ruby` oder `java`
 
 6. Im nächsten Schritt `
 Whether to run untagged jobs [true/false]:
 [false]: true`
 
 7. `Whether to lock Runner to current project [true/false]:
    [true]: true`
 
 8. Runner executor einstellen. In unserem Fall docker
 `Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
  docker`
 
 9. Zuletzt das Docker Image auswählen
 `
 Please enter the Docker image (eg. ruby:2.1):
 alpine:latest`